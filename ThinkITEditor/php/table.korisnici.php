<?php

/*
 * Editor server script for DB table korisnici
 * Created by http://editor.datatables.net/generator
 */

// DataTables PHP library and database connection
include( "lib/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate,
	DataTables\Editor\ValidateOptions;

// The following statement can be removed after the first run (i.e. the database
// table has been created). It is a good idea to do this to help improve
// performance.
$db->sql( "CREATE TABLE IF NOT EXISTS `image` (
	`id` int(10) NOT NULL auto_increment,
	`webPath` varchar(255),
	`fileName` varchar(255),
	`fileSize` varchar(255),
	`systemPath` varchar(255),
	PRIMARY KEY( `id` )
);" );

$db->sql( "CREATE TABLE IF NOT EXISTS `korisnici` (
	`id` int(10) NOT NULL auto_increment,
	`ime` varchar(255),
	`prezime` varchar(255),
	`adresa` varchar(255),
	`grad` varchar(255),
	`email` varchar(255),
	`korisnicko_ime` varchar(255),
	`datum_kreiranja_naloga` datetime,
	`lozinka` varchar(255),
	`image` INT references image(ID),
	PRIMARY KEY( `id` )
);" );



// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'korisnici', 'id' )
	->fields(
		Field::inst( 'ime' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'prezime' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'adresa' ),
		Field::inst( 'grad' ),
		Field::inst( 'email' )
			->validator( Validate::notEmpty() )
			->validator( Validate::email() ),
		Field::inst( 'korisnicko_ime' )
			->validator( Validate::notEmpty() )
			->validator( Validate::unique() ),
		Field::inst( 'datum_kreiranja_naloga' )
			->validator( Validate::dateFormat( 'Y-m-d H:i:s' ) )
			->getFormatter( Format::datetime( 'Y-m-d H:i:s', 'Y-m-d H:i:s' ) )
			->setFormatter( Format::datetime( 'Y-m-d H:i:s', 'Y-m-d H:i:s' ) ),
		Field::inst( 'lozinka' )
			->validator( Validate::notEmpty() ),
		Field::inst( 'image' )
		    ->setFormatter( Format::ifEmpty( null ) )
 			->upload(
 				Upload::inst($_SERVER['DOCUMENT_ROOT'].'/uploads/__ID__.__EXTN__' )
 			 		->db( 'image', 'id', array(
 						'webPath'     => Upload::DB_WEB_PATH,
 						'fileName'    => Upload::DB_FILE_NAME,
 						'fileSize'    => Upload::DB_FILE_SIZE,
 						'systemPath'  => Upload::DB_SYSTEM_PATH
 					) ) ->dbClean( function ( $data ) {
                    // Remove the files from the file system
                    for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
                        unlink( $data[$i]['systemPath'] );
                    }
 
                    // Have Editor remove the rows from the database
                    return true;
                    } )
 					->validator( Validate::fileSize( 5000000, 'Files must be smaller that 5000K' ) )
 					->allowedExtensions( array( 'png', 'jpg', 'jpeg' ), "Please upload an image file" )
 			)
	)
	->process( $_POST )
	->json();
