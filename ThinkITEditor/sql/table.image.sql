-- 
-- Editor SQL for DB table korisnici
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS `image` (
	`id` int(10) NOT NULL auto_increment,
	`webPath` varchar(255),
	`fileName` varchar(255),
	`fileSize` varchar(255),
	`systemPath` varchar(255),
	PRIMARY KEY( `id` )
);