-- 
-- Editor SQL for DB table korisnici
-- Created by http://editor.datatables.net/generator
-- 

CREATE TABLE IF NOT EXISTS `korisnici` (
	`id` int(10) NOT NULL auto_increment,
	`ime` varchar(255),
	`prezime` varchar(255),
	`adresa` varchar(255),
	`grad` varchar(255),
	`email` varchar(255),
	`korisnicko_ime` varchar(255),
	`datum_kreiranja_naloga` datetime,
	`lozinka` varchar(255),
	`image` INT references image(ID),
	PRIMARY KEY( `id` )
);
