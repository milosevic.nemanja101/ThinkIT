
/*
 * Editor client script for DB table korisnici
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	 var editor = new $.fn.dataTable.Editor( {
		ajax: 'php/table.korisnici.php',
		table: '#korisnici',
		i18n: {
        create: {
            button: "Nouveau",
            title:  "Créer nouvelle entrée",
            submit: "Créer"
        },
        edit: {
            button: "Modifier",
            title:  "Modifier entrée",
            submit: "Actualiser"
        },
        remove: {
            button: "Supprimer",
            title:  "Supprimer",
            submit: "Supprimer",
            confirm: {
                _: "Etes-vous sûr de vouloir supprimer %d lignes?",
                1: "Etes-vous sûr de vouloir supprimer 1 ligne?"
            }
        },
        error: {
            system: "Une erreur s’est produite, contacter l’administrateur système"
        },
        multi: {
            title: "Plusieurs valeurs",
            info: "Les éléments sélectionnés contiennent des valeurs différentes pour cette entrée. Pour modifier et mettre tous les éléments pour cette entrée pour la même valeur, cliquez ou appuyez ici, sinon ils vont conserver leurs valeurs individuelles.",
            restore: "Annuler les modifications"
        },
        datetime: {
            previous: 'Précédent',
            next:     'Premier',
            months:   [ 'Janvier', 'Février', 'Mars', 'Avril', 'peut', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
            weekdays: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ]
            }
        },
		fields: [
			{
				"label": "Ime:",
				"name": "ime"
			},
			{
				"label": "Prezime:",
				"name": "prezime"
			},
			{
				"label": "Adresa:",
				"name": "adresa"
			},
			{
				"label": "Grad:",
				"name": "grad"
			},
			{
				"label": "Email:",
				"name": "email"
			},
			{
				"label": "Korisnicko ime:",
				"name": "korisnicko_ime"
			},
			{
				"label": "Datum kreiranja naloga:",
				"name": "datum_kreiranja_naloga",
				"type": "datetime",
				"format": "YYYY-MM-DD HH:mm:ss"
			},
			{
				"label": "Lozinka:",
				"name": "lozinka",
				"type": "password"
			},
			{
            label: "Profilna slika:",
            name: "image",
            type: "upload",
            display: function ( id ) {
                return '<img src="'+editor.file( 'image', id ).webPath+'"/>';
            },
            clearText: "Clear",
            noImageText: 'No image'
            }
		]
	} );

	var table = $('#korisnici').DataTable( {
		dom: 'Bfrtip',
		ajax: 'php/table.korisnici.php',
		columns: [
			{
				"data": "ime"
			},
			{
				"data": "prezime"
			},
			{
				"data": "adresa"
			},
			{
				"data": "grad"
			},
			{
				"data": "email"
			},
			{
				"data": "korisnicko_ime"
			},
			{
				"data": "datum_kreiranja_naloga"
			},
			{
				"data": "lozinka"
			},
		    {
                data: "image",
                render: function ( file_id ) {
                    return file_id ? 
                        '<img style="max-width: 100%;" src="'+editor.file( 'image', file_id ).webPath+'"/>' :
                        null;
                },
                defaultContent: "No image",
                title: "Image"
            }
		],
		select: true,
		lengthChange: false,
		buttons: [
			{ extend: 'create', editor: editor },
			{ extend: 'edit',   editor: editor },
			{ extend: 'remove', editor: editor }
		]
	} );
} );

}(jQuery));

